package ru.alien.shuttle.model;

import org.springframework.stereotype.Component;

@Component
public class HystrixClientFallback implements WikiFeignClient{

    @Override
    public WikiResponseModel discover(String target) {
        return null;
    }
}
