package ru.alien.shuttle.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class WikiResponseModel {

    @JsonProperty("parse")
    private Object parse;

    @JsonProperty("error")
    private Object error;

    public Object getError() {
        return error;
    }

    public Object getParse() {
        return parse;
    }

}
