package ru.alien.shuttle.model;

import javax.persistence.*;

@Entity
@Table(name = "planets_discovered")
public class PlanetEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String name;

    private String discoveryStatus;

    public PlanetEntity(String name, String discoveryStatus) {
        this.discoveryStatus = discoveryStatus;
        this.name = name;
    }

    public PlanetEntity() {
    }
}
