package ru.alien.shuttle.model;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "wiki", url = "https://en.wikipedia.org", fallback = HystrixClientFallback.class)
public interface WikiFeignClient {
    @RequestMapping(value = "/w/api.php?action=parse&section=0&prop=text&page={target}&format=json")
    WikiResponseModel discover(@PathVariable("target") String target);
}

