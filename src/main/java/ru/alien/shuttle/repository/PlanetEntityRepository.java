package ru.alien.shuttle.repository;

import org.springframework.data.repository.CrudRepository;
import ru.alien.shuttle.model.PlanetEntity;

public interface PlanetEntityRepository extends CrudRepository<PlanetEntity, Integer> {
}
